
Recipe App API Proxy

NGINX proxy app for Recipe app API.

## Usage

### Environmental Variables

* 'LISTEN_PORT' - 8000 port default to.
* 'App Host' - Host name which proxy Forward request
* 'app port' - Port of the app to forward defaults 9000


# recipe-app-api-proxy

Recipe API proxy application